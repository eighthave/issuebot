#!/usr/bin/env python3
#
# issuebot_apt_install = python3-jinja2

import jinja2
import inspect
import os
import re
import sys

import defusedxml.ElementTree as XMLElementTree
from distutils.version import LooseVersion
from pathlib import Path
from fdroidserver import common

localmodule = os.path.realpath(
    os.path.join(os.path.dirname(inspect.getfile(inspect.currentframe())), '..')
)
if localmodule not in sys.path:
    sys.path.insert(0, localmodule)
import issuebot

J2_TEMPLATE = """
<h3>Dependencies from Gradle</h3>
<details {% if report_data['found_anti_features'] %}open{% endif %}><summary>{{ ' '.join(report_data['command']) }}</summary><table>
{% if report_data['found_anti_features'] %}
<tr><th>Gradle coordinates</th><th>Anti-Features</th><th>Pattern</th></tr>
{%- else -%}
<tr><th>Gradle coordinates</th><th></th><th></th></tr>
{%- endif -%}
{%- for gradle_coords, url, non_free_pat, anti_features in report_data['rows'] -%}
{%- if anti_features -%}
<tr><td><a href="{{ url }}"><code>{{ gradle_coords }}</code></a></td><td>🚩&nbsp;{{ anti_features }}</td><td><tt>{{ non_free_pat }}</tt></td></tr>
{%- else -%}
<tr><td colspan="3"><a href="{{ url }}"><code>{{ gradle_coords }}</code></a></td></tr>
{%- endif -%}
{%- endfor -%}
</table></details>
"""


class DependenciesFromGradle(issuebot.IssuebotModule):

    gradle_base_dir = None

    def main(self):
        for child in Path(self.source_dir).iterdir():
            if child.name == 'gradle':
                if (child / 'verification-metadata.xml').exists():
                    self.gradle_base_dir = child.parent
                    break
        if not self.gradle_base_dir:
            paths = common.get_all_gradle_and_manifests(self.source_dir)
            # settings.gradle isn't required, so default to source_dir
            self.gradle_base_dir = Path(self.source_dir)
            for path in paths:
                if path.name.startswith('settings.gradle'):
                    self.gradle_base_dir = Path(path).parent
        properties_path = (
            self.gradle_base_dir / 'gradle/wrapper/gradle-wrapper.properties'
        )
        _ignored, properties = issuebot.read_properties(properties_path)
        m = re.match(
            r'http.*/gradle-([0-9]+\.[0-9.]+)', properties.get('distributionUrl', '')
        )
        if m and LooseVersion(m.group(1)) >= LooseVersion('6.2'):
            cmd, data = self.run_gradle_verification_metadata()
        else:
            cmd, data = self.run_gradle_androidDependencies()
        report_data = {'command': cmd, 'data': sorted(data)}
        report_data['rows'] = []
        non_free = []
        trackers = []
        for gradle_coords in report_data['data']:
            anti_features = set()
            non_free_pat = self.non_free_code_match(gradle_coords)
            if non_free_pat:
                self.add_label('non-free')
                anti_features.add('NonFree')
                non_free.append((gradle_coords, non_free_pat.pattern))
            if self.etip_code_search(gradle_coords):
                self.add_label('trackers')
                anti_features.add('Tracker')
                trackers.append((gradle_coords, ''))
            report_data['rows'].append(
                (
                    gradle_coords,
                    self.get_url_from_gradle_coords(gradle_coords),
                    non_free_pat.pattern if non_free_pat else '',
                    ','.join(sorted(anti_features)),
                )
            )

        report_data['found_anti_features'] = bool(non_free or trackers)
        self.reply['reportData'] = report_data
        self.reply['reportData']['problematicEntries'] = dict()
        if non_free:
            self.reply['reportData']['problematicEntries']['non-free'] = non_free
        if trackers:
            self.reply['reportData']['problematicEntries']['tracker'] = trackers
        env = jinja2.Environment(autoescape=True)
        template = env.from_string(J2_TEMPLATE)
        self.reply['report'] = template.render(report_data=report_data)
        self.write_json()

    def get_url_from_gradle_coords(self, gradle_coords):
        url_coords = gradle_coords.replace(':', '/')
        group, artifact, version = gradle_coords.split(':')
        r = self.get_requests_cached_session(allowable_codes=[200, 404]).head(
            'https://repo1.maven.org/maven2/%s/%s/%s/'
            % (group.replace('.', '/'), artifact, version)
        )
        if r.status_code == 200:
            return 'https://central.sonatype.dev/artifact/%s/dependencies' % url_coords
        else:
            return 'https://mvnrepository.com/artifact/' + url_coords

    def run_gradle_verification_metadata(self):
        vmxml = self.gradle_base_dir / 'gradle/verification-metadata.xml'
        cmd = ['gradle/verification-metadata.xml']
        if not vmxml.exists():
            cmd = ['gradlew-fdroid', '--write-verification-metadata', 'sha256', 'help']
            issuebot.run_cli_tool(cmd, self.gradle_base_dir)
        if not vmxml.exists():
            print('Skipping, no gradle/verification-metadata.xml found')
            return
        root = XMLElementTree.parse(vmxml).getroot()
        namespaces = {'': 'https://schema.gradle.org/dependency-verification'}
        dependencies = set()
        for components in root.findall('components', namespaces):
            for component in components.findall('component', namespaces):
                name = component.attrib.get('name')
                group = component.attrib.get('group')
                version = component.attrib.get('version')
                if group and name and version:
                    dependencies.add(':'.join([group, name, version]))
        return cmd, dependencies

    def run_gradle_androidDependencies(self):
        cmd = ['gradlew-fdroid', 'androidDependencies']
        p = issuebot.run_cli_tool(cmd, self.gradle_base_dir)
        dependencies = set()
        if p.stdout:
            pat = re.compile(r'^[+/ -]+(\S+)')
            for line in p.stdout.decode().split('\n'):
                m = pat.search(line)
                if m:
                    gradle_coords = m.group(1).split('@')[0]
                    if len(gradle_coords.split(':')) == 3 and gradle_coords[0] != ':':
                        dependencies.add(gradle_coords)
        return cmd, dependencies


if __name__ == "__main__":
    DependenciesFromGradle().main()
