#! /usr/bin/env python3

import csv
import fileinput
import json
import pycares
import re
import requests
import select
import socket
import sys

IPTOASN_BASE_ENDPOINT_URL = "https://api.iptoasn.com/v1/as/ip/"


class IPLookup(object):
    def __init__(self):
        try:
            from datetime import timedelta
            import requests_cache
            requests_cache.install_cache('api.iptoasn.com.cache',
                                         expire_after=timedelta(weeks=1))
        except ImportError:
            pass

    def lookup(self, ip):
        url = IPTOASN_BASE_ENDPOINT_URL + ip
        r = requests.get(url)
        if r.status_code != 200:
            return None
        info = r.json()
        if info is None or info['announced'] is False:
            return None
        return info


class ASN(object):
    def __init__(self, number, country_code, description):
        self.number = number
        self.description = description


class Host(object):
    def __init__(self, ip=None, name=None, asn=None, query_type=None):
        self.ip = ip
        self.name = name
        self.asn = asn
        self.query_type = query_type

    def __repr__(self):
        return "ip: {}\t name: {} ASN: {}".format(
            self.ip, self.name, self.asn.description)


class ResolverResponse(object):
    def __init__(self, name, channel, res):
        def cb_a(results, err):
            if results is None:
                return
            for result in results:
                res.add((result.host, self.name, 'A'))

        def cb_aaaa(results, err):
            if results is None:
                return
            for result in results:
                res.add((result.host, self.name, 'AAAA'))

        def cb_cname(result, err):
            if result is None:
                return
            res.add((result.cname, self.name, 'CNAME'))
            channel.query(result.cname, pycares.QUERY_TYPE_A, cb_a)
            channel.query(result.cname, pycares.QUERY_TYPE_AAAA, cb_aaaa)
            channel.query(result.cname, pycares.QUERY_TYPE_CNAME, cb_cname)

        self.name = name
        channel.query(name, pycares.QUERY_TYPE_A, cb_a)
        channel.query(name, pycares.QUERY_TYPE_AAAA, cb_aaaa)
        channel.query(name, pycares.QUERY_TYPE_CNAME, cb_cname)


class Resolver(object):
    def __init__(self, timeout=5.0, tries=2, servers=None):
        if servers is None:
            self.channel = pycares.Channel(timeout=timeout, tries=tries)
        else:
            self.channel = pycares.Channel(timeout=timeout, tries=tries,
                                           servers=servers)

    def _wait(self):
        while True:
            read_fds, write_fds = self.channel.getsock()
            if not read_fds and not write_fds:
                break
            timeout = self.channel.timeout()
            if timeout == 0.0:
                self.channel.process_fd(pycares.ARES_SOCKET_BAD,
                                        pycares.ARES_SOCKET_BAD)
                continue
            rlist, wlist, xlist = select.select(read_fds, write_fds, [],
                                                timeout)
            for fd in rlist:
                self.channel.process_fd(fd, pycares.ARES_SOCKET_BAD)
            for fd in wlist:
                self.channel.process_fd(pycares.ARES_SOCKET_BAD, fd)

    def resolve(self, names):
        res = set()
        for name in names:
            response = ResolverResponse(name, self.channel, res)
        self._wait()
        return res


class Extractor(object):
    def __init__(self, txt):
        self.txt = txt

    def extract_names(self):
        label_r = b"[a-z0-9-]{1,63}([.]|\\[.]|,|\[[.]\]|[.]\]| [.])"
        label_last = b"[a-z0-9]{1,16}($|[^a-z0-9])"
        matches = re.findall(b"(" +
                             b"(" + label_r + b"){1,8}" +
                             label_last + b")[.]?",
                             self.txt, re.I)
        names = [re.sub(b",", b".", x[0]).lower() for x in matches]
        names = [re.sub(b"[^a-z0-9-.]", b"", x).decode() for x in names]
        return names

    def extract_ips(self):
        matches = re.findall(b"([^0-9]|^)([0-9]{1,3}(\.|\s*\[\.?\]\s*)" +
                             b"[0-9]{1,3}(\.|\s*\[\.?\]\s*)" +
                             b"[0-9]{1,3}(\.|\s*\[\.?\]\s*)" +
                             b"[0-9]{1,3})([^0-9]|$)", self.txt)
        ips = [re.sub(b"[^0-9.]", b"", x[1]).decode() for x in matches]
        return ips


if __name__ == "__main__":
    ip_lookup = IPLookup()
    resolver = Resolver()
    csvw = csv.writer(sys.stdout, delimiter="\t")
    names, ips = set(), set()

    for line in fileinput.input(mode='rb'):
        extractor = Extractor(line)
        names = names | set(extractor.extract_names())
        ips = ips | set(extractor.extract_ips())

    resolved = resolver.resolve(names)
    hosts_fromnames = set([Host(ip=i, name=n, query_type=q) for i, n, q in resolved])
    hosts_fromips = set([Host(ip=ip) for ip in ips])
    hosts = hosts_fromnames | hosts_fromips

    for host in hosts:
        subnet = ip_lookup.lookup(host.ip)
        asn = ASN(0, "-", "-")
        if subnet:
            asn = ASN(subnet['as_number'], subnet['as_country_code'],
                      "AS{}: {} ({})".format(subnet['as_number'],
                                             subnet['as_description'],
                                             subnet['as_country_code']))
        if not host.name:
            host.name = "-"
        host.asn = asn

    for host in sorted(hosts, key=lambda x: (x.asn.description, x.ip, x.name)):
        csvw.writerow([host.ip, host.name, host.asn.description])
